package org.acme.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.netty.handler.codec.http.HttpResponseStatus;
import org.acme.dataviewer.PostDataViewer;
import org.acme.model.Post;
import org.acme.model.Tag;
import org.acme.repository.PostRepository;
import org.acme.util.SimpleResponse;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.*;

@ApplicationScoped
public class PostService {

    @Inject
    EntityManager entityManager;

    @Inject
    PostRepository postRepo;

    ObjectMapper objectMapper = new ObjectMapper();

    @Transactional
    public SimpleResponse getAllPost() throws Exception {

        List<Post> listPost =  postRepo.getPosts();
        List<PostDataViewer>  listPostDtv = new ArrayList<>();

        //get tags for each post
        if (listPost.size()>0){
            String queryPostTags = "select t.* from activity_schema.tag t \n" +
                                    "inner join activity_schema.post_tags pt on t.id =pt.tag_id \n" +
                                    "where pt.post_id = :postId";
            Query qPostTags = entityManager.createNativeQuery(queryPostTags,Tag.class);
            for (Post post : listPost) {

                PostDataViewer  postDtv = new PostDataViewer();
                qPostTags.setParameter("postId", post.id);
                List<Tag> listPostTags = qPostTags.getResultList();
                List<String> listTags = new ArrayList<>();

                for (Tag tmpTag: listPostTags){
                    listTags.add(tmpTag.label);
                }

                //put all data from post obj to post dataviewer
                postDtv.id = post.id;
                postDtv.title = post.getTitle();
                postDtv.content = post.getContent();
                postDtv.tags = listTags;
                listPostDtv.add(postDtv);
            }
        }

        return new SimpleResponse(HttpStatus.SC_OK, HttpResponseStatus.OK.toString(), listPostDtv);

    }

    @Transactional
    public SimpleResponse getPostByPostId(Long postId) throws Exception {

        Post post =  postRepo.getPost(postId);

        //get tags for each post
        String queryPostTags = "select t.* from activity_schema.tag t \n" +
                                "inner join activity_schema.post_tags pt on t.id =pt.tag_id \n" +
                                "where pt.post_id = :postId";
        Query qPostTags = entityManager.createNativeQuery(queryPostTags,Tag.class);
        PostDataViewer  postDtv = new PostDataViewer();
        qPostTags.setParameter("postId",post.id);
        List<Tag> listPostTags = qPostTags.getResultList();

        List<String> listTags = new ArrayList<>();
        for (Tag tmpTag: listPostTags){
            listTags.add(tmpTag.label);
        }

        //put all data from post obj to post dataviewer
        postDtv.id = post.id;
        postDtv.title = post.getTitle();
        postDtv.content = post.getContent();
        postDtv.tags = listTags;

        return new SimpleResponse(HttpStatus.SC_OK, HttpResponseStatus.OK.toString(), postDtv);
    }

    @Transactional
    public SimpleResponse posts(Object param) throws Exception {

        JsonNode paramNode = objectMapper.valueToTree(param);
        Map<String, Object> req = objectMapper.convertValue(param, new TypeReference<Map<String, Object>>() {});

        String title = req.get("title") == null ? "" : req.get("title").toString();
        String content = req.get("content") == null ? "" : req.get("content").toString();
        JsonNode tagsNode = paramNode.findPath("tags");

        ArrayNode tagsArr = (ArrayNode) tagsNode;
        ObjectReader reader = objectMapper.readerFor(new TypeReference<List<String>>() {});
        List<String> tags = reader.readValue(tagsArr);
        Long postId = Long.valueOf(0);

        //inserting post data to post table
        String queryTxt = "INSERT INTO activity_schema.post(title,content) values(:title,:content)  ON CONFLICT DO nothing returning id;";
        Query q = entityManager.createNativeQuery(queryTxt);
        q.setParameter("title", title);
        q.setParameter("content", content);

        //store the returning id to postId
        List<Object[]> results = q.getResultList();
        if(results.size()>0){
            Object postIdStr = results.get(0);
            String tmpStr = postIdStr.toString();
            postId = Long.parseLong(tmpStr);
        }
        entityManager.clear();

        //inserting tags if not exist in db
        StringBuilder sbTag = new StringBuilder();
        for (String tmpTag : tags) {
            sbTag.append("INSERT INTO activity_schema.tag(label) values('"+tmpTag+"')  ON CONFLICT DO NOTHING;");
        }
        entityManager.createNativeQuery(sbTag.toString()).executeUpdate();
        entityManager.clear();

        //get tag data from db with the tagId
        List<Tag> tagList = Tag.list("label in ?1 ", tags);

        //inserting postsTag relation to a table
        StringBuilder sbPostTags = new StringBuilder();
        for (Tag tmpTag : tagList) {
            Long tagId = tmpTag.id;
            sbPostTags.append("INSERT INTO activity_schema.post_tags(post_id,tag_id) values("+postId.toString()+","+tagId.toString()+")  ON CONFLICT DO NOTHING ;");
        }
        entityManager.createNativeQuery(sbPostTags.toString()).executeUpdate();
        entityManager.clear();

        return new SimpleResponse(HttpStatus.SC_OK, HttpResponseStatus.OK.toString(), null);

    }

    @Transactional
    public SimpleResponse updatePost(Long postId,Object param) throws Exception {

        JsonNode paramNode = objectMapper.valueToTree(param);
        Map<String, Object> req = objectMapper.convertValue(param, new TypeReference<Map<String, Object>>() {});

        String title = req.get("title") == null ? "" : req.get("title").toString();
        String content = req.get("content") == null ? "" : req.get("content").toString();
        JsonNode tagsNode = paramNode.findPath("tags");

        ArrayNode tagsArr = (ArrayNode) tagsNode;
        ObjectReader reader = objectMapper.readerFor(new TypeReference<List<String>>() {});
        List<String> tags = reader.readValue(tagsArr);

        //update post
        Post post = new Post(title,content);
        postRepo.updatePost(postId,post);

        //inserting tag if there is a new tag
        StringBuilder sbTag = new StringBuilder();
        for (String tmpTag : tags) {
            sbTag.append("INSERT INTO activity_schema.tag(label) values('"+tmpTag+"')  ON CONFLICT DO NOTHING;");
        }

        entityManager.createNativeQuery(sbTag.toString()).executeUpdate();
        entityManager.clear();

        //reset tag on post
        String queryTxt = "DELETE FROM activity_schema.post_tags where post_id="+postId.toString();
        entityManager.createNativeQuery(queryTxt).executeUpdate();
        entityManager.clear();

        List<Tag> tagList = Tag.list("label in ?1 ", tags);

        StringBuilder sbPostTags = new StringBuilder();
        for (Tag tmpTag : tagList) {
            Long tagId = tmpTag.id;
            sbPostTags.append("INSERT INTO activity_schema.post_tags(post_id,tag_id) values("+postId.toString()+","+tagId.toString()+")  ON CONFLICT DO NOTHING ;");
        }

        //reinsert new post tag relation
        entityManager.createNativeQuery(sbPostTags.toString()).executeUpdate();
        entityManager.clear();

        return new SimpleResponse(HttpStatus.SC_OK, HttpResponseStatus.OK.toString(), null);

    }

    @Transactional
    public SimpleResponse deletePost(Long postId) throws Exception {

        postRepo.deletePost(postId);
        return new SimpleResponse(HttpStatus.SC_OK, HttpResponseStatus.OK.toString(), null);
    }

}
