package org.acme.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.netty.handler.codec.http.HttpResponseStatus;
import org.acme.dataviewer.PostDataViewer;
import org.acme.model.Post;
import org.acme.model.Tag;
import org.acme.repository.PostRepository;
import org.acme.repository.TagRepository;
import org.acme.util.SimpleResponse;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class TagService {

    @Inject
    TagRepository tagRepo;

    @Transactional
    public SimpleResponse getAllTags() throws Exception {
        return new SimpleResponse(HttpStatus.SC_OK, HttpResponseStatus.OK.toString(), tagRepo.getTags());
    }

    @Transactional
    public SimpleResponse getTagByTagId(Long tagId) throws Exception {
        return new SimpleResponse(HttpStatus.SC_OK, HttpResponseStatus.OK.toString(), tagRepo.getTag(tagId));
    }

    @Transactional
    public SimpleResponse addTags(Tag tag) throws Exception {
        tagRepo.addTag(tag);
        return new SimpleResponse(HttpStatus.SC_OK, HttpResponseStatus.OK.toString(), null);
    }

    @Transactional
    public SimpleResponse updateTag(Long tagId,Tag tag) throws Exception {
        tagRepo.updateTag(tagId,tag);
        return new SimpleResponse(HttpStatus.SC_OK, HttpResponseStatus.OK.toString(), null);
    }

    @Transactional
    public SimpleResponse deleteTag(Long tagId) throws Exception {
        tagRepo.deleteTag(tagId);
        return new SimpleResponse(HttpStatus.SC_OK, HttpResponseStatus.OK.toString(), null);
    }

}
