package org.acme.repository;

import org.acme.model.Post;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;


@Singleton
public class PostRepository {

    @Inject
    EntityManager entityManager;

    public List<Post> getPosts() {
        List<Post> listPost =  Post.listAll();
        return listPost;
    }

    public Post getPost(Long id) {
        return entityManager.find(Post.class, id);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public Post addPost(Post post) {
        entityManager.persist(post);
        return post;
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public void updatePost(Long id, Post post) {
        Post postToUpdate = entityManager.find(Post.class, id);
        if (null != postToUpdate) {
            postToUpdate.setTitle(post.getTitle());
            postToUpdate.setContent(post.getContent());
            postToUpdate.persist();
        } else {
            throw new RuntimeException("No such post available");
        }
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public void deletePost(Long id) {
        Post post = getPost(id);
        entityManager.remove(post);
    }
}