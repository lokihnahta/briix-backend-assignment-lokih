package org.acme.repository;

import org.acme.model.Tag;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Singleton
public class TagRepository {

    @Inject
    EntityManager entityManager;

    public List<Tag> getTags() {
        List<Tag> listTags =  Tag.listAll();
        return listTags;
    }

    public Tag getTag(Long id) {
        return entityManager.find(Tag.class, id);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public Tag addTag(Tag tag) {
        entityManager.persist(tag);
        return tag;
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public void updateTag(Long id, Tag tag) {
        Tag tagToUpdate = entityManager.find(Tag.class, id);
        if (null != tagToUpdate) {
            tagToUpdate.setLabel(tag.getLabel());
            tagToUpdate.persist();
        } else {
            throw new RuntimeException("No such tag available");
        }
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public void deleteTag(Long id) {
        Tag tag = getTag(id);
        entityManager.remove(tag);
    }
}
