package org.acme.util;

public class SimpleResponse {
    private Integer status;
    private String message;
    private Object payload;

    public SimpleResponse(Integer status, String message, Object payload) {
        this.status = status;
        this.message = message;
        this.payload = payload;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getPayload() {
        return this.payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }
}