
package org.acme.controller;

import org.acme.service.PostService;
import org.acme.util.SimpleResponse;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@RequestScoped
@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PostController {

    Logger log = LoggerFactory.getLogger(PostController.class);

    @Inject
    PostService postService;

    @GET
    @Path("/posts")
    public SimpleResponse posts() throws Exception {
        try {
            return postService.getAllPost();
        } catch (Exception ex) {
            log.info("Failed : ", ex);
            return new SimpleResponse(HttpStatus.SC_BAD_REQUEST, ex.getMessage(), null);
        }
    }

    @GET
    @Path("/posts/{id}")
    public SimpleResponse posts(@PathParam("id") Long postId) throws Exception {
        try {
            return postService.getPostByPostId(postId);
        } catch (Exception ex) {
            log.info("Failed : ", ex);
            return new SimpleResponse(HttpStatus.SC_BAD_REQUEST, ex.getMessage(), null);
        }
    }

    @POST
    @Path("/posts")
    public SimpleResponse posts(Object param) throws Exception {
        try {
            return postService.posts(param);
        } catch (Exception ex) {
            log.info("Failed : ", ex);
            return new SimpleResponse(HttpStatus.SC_BAD_REQUEST, ex.getMessage(), null);
        }
    }

    @PUT
    @Path("/posts/{id}")
    public SimpleResponse updatePost(@PathParam("id") Long postId, Object param) throws Exception {
        try {
            return postService.updatePost(postId,param);
        } catch (Exception ex) {
            log.info("Failed : ", ex);
            return new SimpleResponse(HttpStatus.SC_BAD_REQUEST, ex.getMessage(), null);
        }
    }

    @DELETE
    @Path("/posts/{id}")
    public SimpleResponse deletePost(@PathParam("id") Long postId) throws Exception {
        try {
            return postService.deletePost(postId);
        } catch (Exception ex) {
            log.info("Failed : ", ex);
            return new SimpleResponse(HttpStatus.SC_BAD_REQUEST, ex.getMessage(), null);
        }
    }
}

