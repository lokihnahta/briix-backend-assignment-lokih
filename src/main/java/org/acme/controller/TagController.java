package org.acme.controller;

import org.acme.model.Tag;
import org.acme.service.TagService;
import org.acme.util.SimpleResponse;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@RequestScoped
@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TagController {

    Logger log = LoggerFactory.getLogger(PostController.class);

    @Inject
    TagService tagService;

    @GET
    @Path("/tags")
    public SimpleResponse tags() throws Exception {
        try {
            return tagService.getAllTags();
        } catch (Exception ex) {
            log.info("Failed : ", ex);
            return new SimpleResponse(HttpStatus.SC_BAD_REQUEST, ex.getMessage(), null);
        }
    }

    @GET
    @Path("/tags/{id}")
    public SimpleResponse tags(@PathParam("id") Long postId) throws Exception {
        try {
            return tagService.getTagByTagId(postId);
        } catch (Exception ex) {
            log.info("Failed : ", ex);
            return new SimpleResponse(HttpStatus.SC_BAD_REQUEST, ex.getMessage(), null);
        }
    }

    @POST
    @Path("/tags")
    public SimpleResponse addTags(Tag tag) throws Exception {
        try {
            return tagService.addTags(tag);
        } catch (Exception ex) {
            log.info("Failed : ", ex);
            return new SimpleResponse(HttpStatus.SC_BAD_REQUEST, ex.getMessage(), null);
        }
    }

    @PUT
    @Path("/tags/{id}")
    public SimpleResponse updateTag(@PathParam("id") Long postId, Tag tag) throws Exception {
        try {
            return tagService.updateTag(postId,tag);
        } catch (Exception ex) {
            log.info("Failed : ", ex);
            return new SimpleResponse(HttpStatus.SC_BAD_REQUEST, ex.getMessage(), null);
        }
    }

    @DELETE
    @Path("/tags/{id}")
    public SimpleResponse deleteTag(@PathParam("id") Long postId) throws Exception {
        try {
            return tagService.deleteTag(postId);
        } catch (Exception ex) {
            log.info("Failed : ", ex);
            return new SimpleResponse(HttpStatus.SC_BAD_REQUEST, ex.getMessage(),null);
        }
    }
}
