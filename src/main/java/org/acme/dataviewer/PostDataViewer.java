package org.acme.dataviewer;

import org.acme.model.Tag;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PostDataViewer {

    public long id;

    public String title;

    public String content;

    public List<String> tags;

}
